# Dockerized two Microservices



## Introduction

- This is a simple project that consist of two microservices. It can be easily deployed  using Docker Compose.
- It uses MySQL database container for persistent storage 
- The two microservices are creditscore and creditcard. They are spring boot applications

## Creditscore App
- The goal of the crediscore app is to receive GET HTTP requests and to respond to them with json representation of a certain CreditScore object 

## Creditcard App
- The goal of the creditcard app is to receive POST HTTP requests. The request body contains json representation of a CreditCard object 
- The mechanism checks the score of a certain customer - if the score is above 500, credit card is issued, if the score is equal to or below 500, the card is not issued
- In order to get the score of a client, the creditcard app makes a REST API call to the creditscore app. Then check if the value is greater then 500 and save the credit card to the database if it is

## MySQL Container
- The MySQL Container uses volume which is mounted to the /docker-entrypoint-initdb.d directory in the container. Because of this, the sql statements got executed and we have same example test data already

