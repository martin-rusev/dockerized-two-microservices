package docker.example.creditcard.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
public class CreditCard {
    @Id
    private String ssn;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private Date expiryDate;
    private int sec_code;
}
