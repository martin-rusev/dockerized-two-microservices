package docker.example.creditcard.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditScore {
    private String ssn;
    private String firstName;
    private String lastName;
    private int score;
}
