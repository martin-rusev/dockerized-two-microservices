package docker.example.creditcard.controllers;

import docker.example.creditcard.models.CreditCard;
import docker.example.creditcard.models.CreditScore;
import docker.example.creditcard.repositories.CreditCardRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CreditCardController {
    private CreditCardRepo repo;

    @Autowired
    public CreditCardController(CreditCardRepo repo) {
        this.repo = repo;
    }

    @PostMapping("/creditcards")
    public boolean issueCard (@RequestBody CreditCard card){
        if (getScore(card.getSsn()) > 500) {
            repo.save(card);
            return true;
        }
        return false;
    }

    private int getScore(String ssn) {
        RestTemplate template = new RestTemplate();
        CreditScore score = template.getForObject("http://creditscore-app:8080/creditscores/" + ssn, CreditScore.class);
        return score.getScore();
    }
}
