package docker.example.creditcard.repositories;

import docker.example.creditcard.models.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditCardRepo extends JpaRepository<CreditCard, String> {
}
