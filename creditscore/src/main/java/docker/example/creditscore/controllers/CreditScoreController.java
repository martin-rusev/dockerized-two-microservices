package docker.example.creditscore.controllers;

import docker.example.creditscore.models.CreditScore;
import docker.example.creditscore.repositories.CreditScoreRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreditScoreController {
    private CreditScoreRepo creditScoreRepo;

    @Autowired
    public CreditScoreController(CreditScoreRepo creditScoreRepo) {
        this.creditScoreRepo = creditScoreRepo;
    }

    @GetMapping("/creditscores/{ssn}")
    public CreditScore getScore(@PathVariable("ssn") String ssn) {
        return creditScoreRepo.findById(ssn).get();
    }
}
