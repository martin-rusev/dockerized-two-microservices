package docker.example.creditscore.repositories;

import docker.example.creditscore.models.CreditScore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditScoreRepo extends JpaRepository<CreditScore, String> {
}
