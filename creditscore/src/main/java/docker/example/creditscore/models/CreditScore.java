package docker.example.creditscore.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class CreditScore {
    @Id
    private String ssn;
    private String firstName;
    private String lastName;
    private int score;
}
